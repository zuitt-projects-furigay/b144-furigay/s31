// MODEL

const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({

	name: String,
	status: {
		type: String,
		status: "pending"
	}
})

//"module.exports" is a way for our js to treat this value as a "package" that can be used by other files.
module.exports = mongoose.model("Task", taskSchema);

