//Set up the dependencies
const express = require('express')
const mongoose = require('mongoose')

// This allows us to use all the rouutes defined in 'taskRoute.js'
const taskRoute = require('./routes/taskRoute')

// Server setup

const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended: true}))


// DB connection
mongoose.connect("mongodb+srv://adriannfurigay:qwerty123@wdc028-course-booking.umyxd.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});


let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We're connected to the cloud database"))


// Routes(Base URI for task route)
// allows all the task routes created in the tasskRoutse.js file too use "/task" route

app.use("/tasks", taskRoute)
// https://localhost:3001/tasks





app.listen(port, () => console.log(`Now listening to port ${port}`))

