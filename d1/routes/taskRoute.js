// 
const express = require('express');
const taskController = require('../controllers/taskController')
// Create a Router instance that function as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create toutes for our application.
const router = express.Router();

// Route to get all tasks
//http://localhost:3001/tasks
router.get("/", (req, res) =>{
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController))
})


// ----------------------------

// CREATE (POST)
// Create a route for creating a task
router.post ("/", (req, res) =>{
	taskController.createTask(req.body).then(result => res.send(result));
})


// ----------------------------


// DELETE
// Route for deleting a task.
// (:) - wildcard
// http://localhost:3001/task/:id
// The colon (:) is an identiifier that hels create a dynamic route which allows us to use supply information in the URL
//The word that comes after the colon symbol wwill be the name the name of the URL parameter
//":id" is called wildcard where you can put any valu, it then creates a link between "id" param in the url and the value provided in the URL 

router.delete("/:id", (req, res) => {
	// URL parameter values are accessed via the request object's "params" property 
	// the property name of this object will match the giiven URL parameter name
	// In this case "id " is the name of the paramater
	taskController.deleteTask(req.params.id).then(result => res.send(result))

})


// -------------------------


// UPDATE
// Update a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result))
})








// --------------------------------------------------


// ACTIVITY CODE

// SPECIFIC TASK
router.get("/:id", (req, res) => {
	taskController.getSpecificTasks(req.params.id).then(result => res.send(result))
})



// CHANGING STATUS

router.patch("/:id", (req, res) => {
	taskController.updateStatus(req.params.id, req.body).then(result => res.send(result))
})


module.exports = router;