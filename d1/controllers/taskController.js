// Controllers contain the functions and businnes logic oof our express js app.
// meaninf all the operations can do will be placed in this file.
const Task = require('../models/task');

// GET
// Controller function for getting all the task

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
}


// -------------------


// CREATE (POST)
//Controller function for creating a task 
module.exports.createTask = (requestBody) => {
	//Create a task object based on the mongoose model "Task"
	let newTask = new Task({
		name: requestBody.name,
		status: requestBody.status
	})

	// Save
	// The 'then' method will  accept the 2 parameters
		//the first parameter will store the result returned by the mongoose 'save' method
		// the second param will store the "error" object
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error)
			// if an error is encountered, the "return" statement will prevent any other line or code below and within the same code block from executing.
			// Since the following return statement is nested within the "then" method chained to the "save" method, they do not prevent each other from executing code
			// the else statement will no longer evaluated
			return false;
		}else {
			// if save is successful return the new task object
			return task;
		}
	})
}


// -----------------------

// DELETING
// Deleting a task
/*
Business Logic
1. Look or find the task with the corresponding id provided in the URL/route
2. Delete the task

*/

module.exports.deleteTask = (taskId) => {
	// tthe "findByIdAndRemove" Mongoose method will look for a task with the same id provided from the URL and remove/delete the document from MongoDB it look for the document using the "_id" field
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err) {
			console.log(err);
			return false;

		}else {
			return removedTask;
		}
	})
}




// -------------------------


// UPDATE
// Updating a task
/*
BUSINESS LOGIC
1. Get the task with the ID
2. Replace the task's name returned from the database with the "name" property from the request body
3. Save the task


*/


module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, error) => {
		// if an error is encountered, return a false
		if(error){
			console.log(error);
			return false;
		}

		// Results 

		// Its name property will be reassigned the value of the "name" received from the request
		result.name = newContent.name;

		return result.save().then((updatedTask, error) => {
			if(error){
				console.log(error);
			}else{
				return updatedTask;
			}
		})
	})
}






// -----------------------------------


// ACTIVITY CODE

// SPECIFIC TASK
module.exports.getSpecificTasks = (userId) => {
	return Task.findById(userId).then((result) => {
		return result;
	})
}



// CHANGING STATUS

module.exports.updateStatus = (userId, newStatus) => {
	return Task.findById(userId).then((result, error) => {
		if(error){
			console.log(error);
			return false;
		}

		result.status = newStatus.status;

		return result.save().then((updateStatus, error) => {
			if(error){
				console.log(error);
			}else {
				return updateStatus;
			}
		})
	})
}